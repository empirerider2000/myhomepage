<?php
    include('assets/core/db.php');

    if(isset($_GET['p'])) {
        $p = htmlspecialchars($_GET['p']);
    }else {
        $p = 'index';
    }
?>
<?php include('assets/core/ifcss.php'); ?>
<!DOCTYPE html>
<html lang='en'>
<?php include('assets/core/head.php'); ?>
<body>
    <?php include('assets/templates/navbar.php'); ?>
    <?php include('assets/templates/' . $p . '.php'); ?>
    <script src="assets\js\fix-navbar-css.js"></script>
</body>
</html>